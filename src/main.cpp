#define VK_USE_PLATFORM_WIN32_KHR
#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include "vulkan.h"
#include <errno.h>

float vertices[] = { -1.f, -1.f, 1.f, -1.f, -1.f, 1.f };

static VKAPI_ATTR VkBool32 VKAPI_CALL DebugReportCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT /*objectType*/, uint64_t /*object*/, size_t /*location*/, int32_t /*messageCode*/, const char * /*pLayerPrefix*/, const char *pMessage, void * /*pUserData*/)
{
	printf("%d: %s\n", flags, pMessage);
	return VK_FALSE;
}

static inline bool _ReadFile(const char *pFileName, char **ppOutput, uint64_t *pOutputLength)
{
	errno = 0;

	FILE *pFile;
	pFile = fopen(pFileName, "rb");

	if (pFile == nullptr)
	{
		printf("Error number: %d\n", errno);
		return false;
	}

	fseek(pFile, 0, SEEK_END);
	(*pOutputLength) = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	char *pOutput = (char*)malloc((size_t)(*pOutputLength) + 1);
	fread(pOutput, 1, (size_t)(*pOutputLength), pFile);
	fclose(pFile);

	(*ppOutput) = pOutput;
	return true;
}

static LRESULT __stdcall WindowProcedure(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	default:
		return DefWindowProc(windowHandle, message, wParam, lParam);
		break;
	}
}

static inline bool _GetMemoryAllocateInfoForMemoryRequirements(VkPhysicalDeviceMemoryProperties *pPhysicalDeviceMemoryProperties, VkMemoryRequirements *pMemoryRequirements, VkMemoryAllocateInfo *pMemoryAllocateInfo, VkMemoryPropertyFlags requiredMemoryPropertyFlags)
{
	pMemoryAllocateInfo->sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	pMemoryAllocateInfo->pNext = nullptr;
	pMemoryAllocateInfo->allocationSize = pMemoryRequirements->size;

	uint32_t memoryTypeBits = pMemoryRequirements->memoryTypeBits;

	for (uint32_t i = 0; i < pPhysicalDeviceMemoryProperties->memoryTypeCount; ++i)
	{
		uint32_t heapIndex = pPhysicalDeviceMemoryProperties->memoryTypes[i].heapIndex;
		if ((memoryTypeBits & 1) == 1 && pMemoryRequirements->size <= pPhysicalDeviceMemoryProperties->memoryHeaps[heapIndex].size)
		{
			if ((requiredMemoryPropertyFlags & pPhysicalDeviceMemoryProperties->memoryTypes[i].propertyFlags) == requiredMemoryPropertyFlags)
			{
				pMemoryAllocateInfo->memoryTypeIndex = i;
				return true;
			}
		}

		memoryTypeBits >>= 1;

		/*if ((pMemoryRequirements->memoryTypeBits & pPhysicalDeviceMemoryProperties->memoryTypes[i].propertyFlags) == pMemoryRequirements->memoryTypeBits)
		{
			uint32_t heapIndex = pPhysicalDeviceMemoryProperties->memoryTypes[i].heapIndex;

			if (pMemoryRequirements->size <= pPhysicalDeviceMemoryProperties->memoryHeaps[heapIndex].size)
			{
				pMemoryAllocateInfo->memoryTypeIndex = heapIndex;
				return true;
			}
		}*/
	}

	return false;
}

static inline void _CreateDeviceVertexBuffer(VkDevice device, VkQueue queue, float *pVertices, uint32_t vertexCount, VkAllocationCallbacks *pAllocationCallbacks, VkPhysicalDeviceMemoryProperties *pPhysicalDeviceMemoryProperties, VkCommandPool commandPool, VkBuffer *pBuffer)
{
	VkResult result;

	VkBufferCreateInfo bufferCreateInfo;
	bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCreateInfo.pNext = nullptr;
	bufferCreateInfo.flags = 0;
	bufferCreateInfo.size = sizeof(float) * 2 * vertexCount;
	bufferCreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	bufferCreateInfo.queueFamilyIndexCount = 0;
	bufferCreateInfo.pQueueFamilyIndices = nullptr;

	VkBuffer stagingBuffer;
	result = vkCreateBuffer(device, &bufferCreateInfo, pAllocationCallbacks, &stagingBuffer);

	if (result < 0)
		exit(1);

	VkMemoryRequirements memoryRequirements;
	vkGetBufferMemoryRequirements(device, stagingBuffer, &memoryRequirements);

	VkMemoryAllocateInfo memoryAllocateInfo;
	if (!_GetMemoryAllocateInfoForMemoryRequirements(pPhysicalDeviceMemoryProperties, &memoryRequirements, &memoryAllocateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
		exit(1);

	VkDeviceMemory deviceStagingMemory;
	result = vkAllocateMemory(device, &memoryAllocateInfo, pAllocationCallbacks, &deviceStagingMemory);

	if (result < 0)
		exit(1);

	float *pMappedVertexStagingBuffer;
	result = vkMapMemory(device, deviceStagingMemory, 0, memoryRequirements.size, 0, (void**)&pMappedVertexStagingBuffer);

	if (result < 0)
		exit(1);

	memcpy(pMappedVertexStagingBuffer, pVertices, sizeof(float) * 2 * vertexCount);

	vkUnmapMemory(device, deviceStagingMemory);

	vkBindBufferMemory(device, stagingBuffer, deviceStagingMemory, 0);

	bufferCreateInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

	VkBuffer buffer;
	result = vkCreateBuffer(device, &bufferCreateInfo, pAllocationCallbacks, &buffer);

	if (result < 0)
		exit(1);

	vkGetBufferMemoryRequirements(device, buffer, &memoryRequirements);

	if (!_GetMemoryAllocateInfoForMemoryRequirements(pPhysicalDeviceMemoryProperties, &memoryRequirements, &memoryAllocateInfo, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT))
		exit(1);

	VkDeviceMemory deviceMemory;
	result = vkAllocateMemory(device, &memoryAllocateInfo, pAllocationCallbacks, &deviceMemory);

	if (result < 0)
		exit(1);

	result = vkBindBufferMemory(device, buffer, deviceMemory, 0);

	if (result < 0)
		exit(1);

	VkCommandBufferAllocateInfo commandBufferAllocateInfo;
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.pNext = nullptr;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(device, &commandBufferAllocateInfo, &commandBuffer);

	VkCommandBufferBeginInfo commandBufferBeginInfo;
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.pNext = nullptr;
	commandBufferBeginInfo.flags = 0;
	commandBufferBeginInfo.pInheritanceInfo = nullptr;

	VkBufferCopy bufferCopy;
	bufferCopy.srcOffset = 0;
	bufferCopy.dstOffset = 0;
	bufferCopy.size = sizeof(float) * 2 * vertexCount;

	vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);
	vkCmdCopyBuffer(commandBuffer, stagingBuffer, buffer, 1, &bufferCopy);

	result = vkEndCommandBuffer(commandBuffer);

	if (result < 0)
		exit(1);

	VkSubmitInfo submitInfo;
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.pNext = nullptr;
	submitInfo.waitSemaphoreCount = 0;
	submitInfo.pWaitSemaphores = nullptr;
	submitInfo.pWaitDstStageMask = nullptr;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;
	submitInfo.signalSemaphoreCount = 0;
	submitInfo.pSignalSemaphores = nullptr;

	result = vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);

	if (result < 0)
		exit(1);

	result = vkQueueWaitIdle(queue);

	if (result < 0)
		exit(1);

	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
	vkDestroyBuffer(device, stagingBuffer, pAllocationCallbacks);
	vkFreeMemory(device, deviceStagingMemory, pAllocationCallbacks);

	(*pBuffer) = buffer;
}

int main()
{
	VkResult result;
	
	VkApplicationInfo applicationInfo;
	applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	applicationInfo.pNext = nullptr;
	applicationInfo.pApplicationName = "Hello Vulkan";
	applicationInfo.applicationVersion = 1;
	applicationInfo.pEngineName = "Hello Vulkan Engine";
	applicationInfo.engineVersion = 1;
	applicationInfo.apiVersion = VK_API_VERSION;

	char *pInstanceExtensionNames[] = { VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_WIN32_SURFACE_EXTENSION_NAME, VK_EXT_DEBUG_REPORT_EXTENSION_NAME };

	//uint32_t layerCount = 0;
	//vkEnumerateInstanceLayerProperties(&layerCount, nullptr);
	//VkLayerProperties *pLayers = (VkLayerProperties*)malloc(sizeof(VkLayerProperties) * layerCount);
	//vkEnumerateInstanceLayerProperties(&layerCount, pLayers);
	//
	//char **ppLayerNames = (char**)malloc(sizeof(char**) * layerCount);
	//
	//for (uint32_t i = 0; i < layerCount; ++i)
	//{
	//	ppLayerNames[i] = pLayers[i].layerName;
	//}

	char *pInstanceLayerNames[] = { "VK_LAYER_LUNARG_standard_validation" };

	VkInstanceCreateInfo instanceCreateInfo;
	instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceCreateInfo.pNext = nullptr;
	instanceCreateInfo.flags = 0;
	instanceCreateInfo.pApplicationInfo = &applicationInfo;
	instanceCreateInfo.enabledLayerCount = 1;
	instanceCreateInfo.ppEnabledLayerNames = pInstanceLayerNames;
	instanceCreateInfo.enabledExtensionCount = 3;
	instanceCreateInfo.ppEnabledExtensionNames = pInstanceExtensionNames;
	
	VkInstance instance;
	result = vkCreateInstance(&instanceCreateInfo, nullptr, &instance);

	if (result < 0)
		exit(1);
	
	PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT"));
	//PFN_vkDebugReportMessageEXT vkDebugReportMessageEXT = reinterpret_cast<PFN_vkDebugReportMessageEXT>(vkGetInstanceProcAddr(instance, "vkDebugReportMessageEXT"));
	//PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT"));

	VkDebugReportCallbackCreateInfoEXT debugReportCallbackCreateInfo;
	debugReportCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
	debugReportCallbackCreateInfo.pNext = nullptr;
	debugReportCallbackCreateInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
	debugReportCallbackCreateInfo.pfnCallback = &DebugReportCallback;
	debugReportCallbackCreateInfo.pUserData = nullptr;

	VkDebugReportCallbackEXT callback;
	result = vkCreateDebugReportCallbackEXT(instance, &debugReportCallbackCreateInfo, nullptr, &callback);

	uint32_t physicalDeviceCount;
	vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, nullptr);
	VkPhysicalDevice *pPhysicalDevices = (VkPhysicalDevice*)malloc(sizeof(VkPhysicalDevice) * physicalDeviceCount);
	result = vkEnumeratePhysicalDevices(instance, &physicalDeviceCount, pPhysicalDevices);

	VkPhysicalDeviceMemoryProperties *pPhysicalDeviceMemoryProperties = (VkPhysicalDeviceMemoryProperties*)malloc(sizeof(VkPhysicalDeviceMemoryProperties) * physicalDeviceCount);

	for (uint32_t i = 0; i < physicalDeviceCount; ++i)
	{
		vkGetPhysicalDeviceMemoryProperties(pPhysicalDevices[i], pPhysicalDeviceMemoryProperties);
	}

	if (result < 0)
		exit(1);
	
	uint32_t deviceCount = physicalDeviceCount;
	VkDevice *pDevices = (VkDevice*)malloc(sizeof(VkDevice) * deviceCount);
	
	uint32_t *pQueueFamilyCounts = (uint32_t*)malloc(sizeof(uint32_t) * deviceCount);
	uint32_t queueFamilyCount = 0;

	for (uint32_t i = 0; i < deviceCount; ++i)
	{
		vkGetPhysicalDeviceQueueFamilyProperties(pPhysicalDevices[i], pQueueFamilyCounts + i, nullptr);
		queueFamilyCount += pQueueFamilyCounts[i];
	}

	VkQueueFamilyProperties *pQueueFamilies = (VkQueueFamilyProperties*)malloc(sizeof(VkQueueFamilyProperties) * queueFamilyCount);
	uint32_t queuePriorityCount = 0;

	VkDeviceQueueCreateInfo *pQueueCreateInfos = (VkDeviceQueueCreateInfo*)malloc(sizeof(VkDeviceQueueCreateInfo) * queueFamilyCount);
	uint32_t queueCount = 0;

	for (uint32_t i = 0, queueFamilyIterator = 0; i < deviceCount; queueFamilyIterator += pQueueFamilyCounts[i], ++i)
	{
		vkGetPhysicalDeviceQueueFamilyProperties(pPhysicalDevices[i], pQueueFamilyCounts + i, pQueueFamilies + queueFamilyIterator);

		for (uint32_t j = 0; j < pQueueFamilyCounts[i]; ++j)
		{
			if (queuePriorityCount < pQueueFamilies[i].queueCount)
				queuePriorityCount = pQueueFamilies[i].queueCount;

			queueCount += pQueueFamilies[i].queueCount;
		}
	}

	float *pQueuePriorities = (float*)malloc(sizeof(float) * queuePriorityCount);
	VkQueue *pQueues = (VkQueue*)malloc(sizeof(VkQueue) * queueCount);

	for (uint32_t i = 0; i < queuePriorityCount; ++i)
		pQueuePriorities[i] = 1.f;

	for (uint32_t i = 0, deviceQueueFamilyOffset = 0, queueOffset = 0; i < deviceCount; deviceQueueFamilyOffset += pQueueFamilyCounts[i], ++i)
	{
		for (uint32_t j = 0; j < pQueueFamilyCounts[i]; ++j)
		{
			uint32_t queueFamilyIndex = deviceQueueFamilyOffset + j;
			pQueueCreateInfos[queueFamilyIndex].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
			pQueueCreateInfos[queueFamilyIndex].pNext = nullptr;
			pQueueCreateInfos[queueFamilyIndex].flags = 0;
			pQueueCreateInfos[queueFamilyIndex].queueFamilyIndex = j;
			pQueueCreateInfos[queueFamilyIndex].queueCount = pQueueFamilies[queueFamilyIndex].queueCount;
			pQueueCreateInfos[queueFamilyIndex].pQueuePriorities = pQueuePriorities;
		}
		
		char *pDeviceLayerNames[] = { "VK_LAYER_LUNARG_standard_validation" };
		char *pDeviceExtensionNames[] = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

		VkPhysicalDeviceFeatures physicalDeviceFeatures;
		vkGetPhysicalDeviceFeatures(pPhysicalDevices[i], &physicalDeviceFeatures);

		VkDeviceCreateInfo deviceCreateInfo;
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.pNext = nullptr;
		deviceCreateInfo.flags = 0;
		deviceCreateInfo.queueCreateInfoCount = pQueueFamilyCounts[i];
		deviceCreateInfo.pQueueCreateInfos = pQueueCreateInfos + deviceQueueFamilyOffset;
		deviceCreateInfo.enabledLayerCount = 1;
		deviceCreateInfo.ppEnabledLayerNames = pDeviceLayerNames;
		deviceCreateInfo.enabledExtensionCount = 1;
		deviceCreateInfo.ppEnabledExtensionNames = pDeviceExtensionNames;
		deviceCreateInfo.pEnabledFeatures = nullptr;

		result = vkCreateDevice(pPhysicalDevices[i], &deviceCreateInfo, nullptr, pDevices + i);

		if (result < 0)
			exit(1);

		for (uint32_t j = 0; j < pQueueFamilyCounts[i]; ++j)
		{
			uint32_t queueFamilyIndex = deviceQueueFamilyOffset + j;
			for (uint32_t k = 0; k < pQueueFamilies[queueFamilyIndex].queueCount; ++k)
			{
				vkGetDeviceQueue(pDevices[i], queueFamilyIndex, k, pQueues + queueOffset);
				++queueOffset;
			}
		}
	}

	uint32_t commandPoolDeviceIndex;
	uint32_t commandPoolQueueFamilyIndex;
	bool foundGraphicsQueueFamily = false;

	for (uint32_t i = 0, queueFamilyIterator = 0; i < deviceCount; ++i)
	{
		for (uint32_t j = 0; j < pQueueFamilyCounts[i]; ++j, ++queueFamilyIterator)
		{
			if (pQueueFamilies[queueFamilyIterator].queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				commandPoolDeviceIndex = i;
				commandPoolQueueFamilyIndex = j;
				foundGraphicsQueueFamily = true;
			}
		}
	}

	if (!foundGraphicsQueueFamily)
		exit(1);

	VkCommandPoolCreateInfo commandPoolCreateInfo;
	commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	commandPoolCreateInfo.pNext = nullptr;
	commandPoolCreateInfo.flags = 0;
	commandPoolCreateInfo.queueFamilyIndex = commandPoolQueueFamilyIndex;

	VkCommandPool commandPool;
	vkCreateCommandPool(pDevices[commandPoolDeviceIndex], &commandPoolCreateInfo, nullptr, &commandPool);

	VkCommandBufferAllocateInfo commandBufferAllocateInfo;
	commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	commandBufferAllocateInfo.pNext = nullptr;
	commandBufferAllocateInfo.commandPool = commandPool;
	commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	commandBufferAllocateInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer;
	vkAllocateCommandBuffers(pDevices[commandPoolDeviceIndex], &commandBufferAllocateInfo, &commandBuffer);

	VkAttachmentDescription attachmentDescription;
	attachmentDescription.flags = 0;
	attachmentDescription.format = VK_FORMAT_R8G8B8A8_UNORM;
	attachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
	attachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachmentDescription.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference colorAttachmentReference;
	colorAttachmentReference.attachment = 0;
	colorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpassDescription;
	subpassDescription.flags = 0;
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.inputAttachmentCount = 0;
	subpassDescription.pInputAttachments = nullptr;
	subpassDescription.colorAttachmentCount = 1;
	subpassDescription.pColorAttachments = &colorAttachmentReference;
	subpassDescription.pResolveAttachments = nullptr;
	subpassDescription.pDepthStencilAttachment = nullptr;
	subpassDescription.preserveAttachmentCount = 0;
	subpassDescription.pPreserveAttachments = nullptr;

	VkRenderPassCreateInfo renderPassCreateInfo;
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.pNext = nullptr;
	renderPassCreateInfo.flags = 0;
	renderPassCreateInfo.attachmentCount = 1;
	renderPassCreateInfo.pAttachments = &attachmentDescription;
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pSubpasses = &subpassDescription;
	renderPassCreateInfo.dependencyCount = 0;
	renderPassCreateInfo.pDependencies = nullptr;

	VkRenderPass renderPass;
	result = vkCreateRenderPass(pDevices[commandPoolDeviceIndex], &renderPassCreateInfo, nullptr, &renderPass);

	if (result < 0)
		exit(1);

	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(pPhysicalDevices[commandPoolDeviceIndex], &physicalDeviceProperties);

	VkFormatProperties formatProperties;
	vkGetPhysicalDeviceFormatProperties(pPhysicalDevices[commandPoolDeviceIndex], VK_FORMAT_R8G8B8A8_UINT, &formatProperties);

	printf("Format properties for VK_FORMAT_R8G8B8A8_UINT: ");
	for (uint32_t i = 0; i < 32; ++i)
	{
		printf("%d, ", formatProperties.optimalTilingFeatures & (1 << i));
	}
	printf("\n");

	for (uint32_t i = 0; i < 32; ++i)
	{
		printf("%d, ", physicalDeviceProperties.limits.framebufferColorSampleCounts & (1 << i));
	}
	printf("\n");
	printf("Max framebuffer size: %d, %d, %d\n", physicalDeviceProperties.limits.maxFramebufferWidth, physicalDeviceProperties.limits.maxFramebufferHeight, physicalDeviceProperties.limits.maxFramebufferLayers);

	VkExtent3D extent;
	extent.width = 256;
	extent.height = 256;
	extent.depth = 1;

	//VkImageCreateInfo imageCreateInfo;
	//imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	//imageCreateInfo.pNext = nullptr;
	//imageCreateInfo.flags = VK_IMAGE_CREATE_MUTABLE_FORMAT_BIT;
	//imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
	//imageCreateInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	//imageCreateInfo.extent = extent;
	//imageCreateInfo.mipLevels = 1;
	//imageCreateInfo.arrayLayers = 1;
	//imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	//imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	//imageCreateInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
	//imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	//imageCreateInfo.queueFamilyIndexCount = 0;
	//imageCreateInfo.pQueueFamilyIndices = nullptr;
	//imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	//
	//VkImage image;
	//result = vkCreateImage(pDevices[commandPoolDeviceIndex], &imageCreateInfo, nullptr, &image);
	//
	//if (result < 0)
	//	exit(1);
	//
	//VkMemoryRequirements imageMemoryRequirements;
	//vkGetImageMemoryRequirements(pDevices[commandPoolDeviceIndex], image, &imageMemoryRequirements);
	//
	//VkMemoryAllocateInfo memoryAllocateInfo;
	//
	//if (!_GetMemoryAllocateInfoForMemoryRequirements(pPhysicalDeviceMemoryProperties + commandPoolDeviceIndex, &imageMemoryRequirements, &memoryAllocateInfo, 0))
	//	exit(1);
	//
	//VkDeviceMemory imageDeviceMemory;
	//result = vkAllocateMemory(pDevices[commandPoolDeviceIndex], &memoryAllocateInfo, nullptr, &imageDeviceMemory);
	//
	//if (result < 0)
	//	exit(1);
	//
	//result = vkBindImageMemory(pDevices[commandPoolDeviceIndex], image, imageDeviceMemory, 0);
	//
	//if (result < 0)
	//	exit(1);


	WNDCLASS windowsClass = {};
	const wchar_t CLASS_NAME[] = L"HelloVulkan";
	HINSTANCE hInstance = GetModuleHandle(nullptr);;

	windowsClass.lpfnWndProc = WindowProcedure;
	windowsClass.hInstance = hInstance;
	windowsClass.lpszClassName = L"HelloVulkan";

	RegisterClass(&windowsClass);

	HWND windowHandle = CreateWindowEx(0, CLASS_NAME, L"Hello Vulkan", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 1280, 720, nullptr, nullptr, hInstance, nullptr);

	if (windowHandle == nullptr)
		exit(1);

	ShowWindow(windowHandle, SW_SHOW);

	VkWin32SurfaceCreateInfoKHR surfaceCreateInfo;
	surfaceCreateInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	surfaceCreateInfo.pNext = nullptr;
	surfaceCreateInfo.flags = 0;
	surfaceCreateInfo.hinstance = hInstance;
	surfaceCreateInfo.hwnd = windowHandle;

	VkSurfaceKHR surface;
	result = vkCreateWin32SurfaceKHR(instance, &surfaceCreateInfo, nullptr, &surface);

	if (result < 0)
		exit(1);

	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(pPhysicalDevices[commandPoolDeviceIndex], surface, &surfaceCapabilities);

	bool surfaceSupportingQueueFamilyFound = false;
	uint32_t surfaceSupportingQueueFamilyDevice = 0;
	uint32_t surfaceSupportingQueueFamilyIndex = 0;

	for (uint32_t i = 0; i < deviceCount && !surfaceSupportingQueueFamilyFound; ++i)
	{
		for (uint32_t j = 0; j < pQueueFamilyCounts[i]; ++j)
		{
			VkBool32 found;
			vkGetPhysicalDeviceSurfaceSupportKHR(pPhysicalDevices[i], j, surface, &found);

			if (found)
			{
				surfaceSupportingQueueFamilyFound = true;
				surfaceSupportingQueueFamilyDevice = i;
				surfaceSupportingQueueFamilyIndex = j;
				break;
			}
		}
	}

	if (!surfaceSupportingQueueFamilyFound)
		exit(1);

	uint32_t surfaceFormatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(pPhysicalDevices[commandPoolDeviceIndex], surface, &surfaceFormatCount, nullptr);
	VkSurfaceFormatKHR *pSurfaceFormats = (VkSurfaceFormatKHR*)malloc(sizeof(VkSurfaceFormatKHR) * surfaceFormatCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(pPhysicalDevices[commandPoolDeviceIndex], surface, &surfaceFormatCount, pSurfaceFormats);

	uint32_t surfacePresentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(pPhysicalDevices[commandPoolDeviceIndex], surface, &surfacePresentModeCount, nullptr);
	VkPresentModeKHR *pSurfacePresenModes = (VkPresentModeKHR*)malloc(sizeof(VkPresentModeKHR) * surfacePresentModeCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(pPhysicalDevices[commandPoolDeviceIndex], surface, &surfacePresentModeCount, pSurfacePresenModes);

	VkSwapchainCreateInfoKHR swapchainCreateInfo;
	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.pNext = nullptr;
	swapchainCreateInfo.flags = 0;
	swapchainCreateInfo.surface = surface;
	swapchainCreateInfo.minImageCount = surfaceCapabilities.minImageCount;
	swapchainCreateInfo.imageFormat = pSurfaceFormats[0].format;
	swapchainCreateInfo.imageColorSpace = pSurfaceFormats[0].colorSpace;
	swapchainCreateInfo.imageExtent = surfaceCapabilities.currentExtent;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchainCreateInfo.queueFamilyIndexCount = 0;
	swapchainCreateInfo.pQueueFamilyIndices = nullptr;
	swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	swapchainCreateInfo.presentMode = pSurfacePresenModes[0];
	swapchainCreateInfo.clipped = true;
	swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

	VkSwapchainKHR swapchain;
	result = vkCreateSwapchainKHR(pDevices[commandPoolDeviceIndex], &swapchainCreateInfo, nullptr, &swapchain);

	if (result < 0)
		exit(1);

	VkFenceCreateInfo swapchainNextImageFenceCreateInfo;
	swapchainNextImageFenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	swapchainNextImageFenceCreateInfo.pNext = nullptr;
	swapchainNextImageFenceCreateInfo.flags = 0;

	VkFence swapchainNextImageFence;
	result = vkCreateFence(pDevices[commandPoolDeviceIndex], &swapchainNextImageFenceCreateInfo, nullptr, &swapchainNextImageFence);

	if (result < 0)
		exit(1);

	uint32_t swapchainImageCount;
	vkGetSwapchainImagesKHR(pDevices[commandPoolDeviceIndex], swapchain, &swapchainImageCount, nullptr);
	VkImage *pSwapchainImages = (VkImage*)malloc(sizeof(VkImage) * swapchainImageCount);
	result = vkGetSwapchainImagesKHR(pDevices[commandPoolDeviceIndex], swapchain, &swapchainImageCount, pSwapchainImages);

	if (result < 0)
		exit(1);

	uint32_t swapchainNextImageIndex;
	result = vkAcquireNextImageKHR(pDevices[commandPoolDeviceIndex], swapchain, 1000000, VK_NULL_HANDLE, swapchainNextImageFence, &swapchainNextImageIndex);

	if (result < 0)
		exit(1);


	VkImageSubresourceRange imageSubresourceRange;
	imageSubresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	imageSubresourceRange.baseMipLevel = 0;
	imageSubresourceRange.levelCount = 1;
	imageSubresourceRange.baseArrayLayer = 0;
	imageSubresourceRange.layerCount = 1;

	VkImageViewCreateInfo imageViewCreateInfo;
	imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	imageViewCreateInfo.pNext = nullptr;
	imageViewCreateInfo.flags = 0;
	imageViewCreateInfo.image = pSwapchainImages[swapchainNextImageIndex];
	imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	imageViewCreateInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	imageViewCreateInfo.components = { VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY };
	imageViewCreateInfo.subresourceRange = imageSubresourceRange;

	VkImageView imageView;
	result = vkCreateImageView(pDevices[commandPoolDeviceIndex], &imageViewCreateInfo, nullptr, &imageView);

	if (result < 0)
		exit(1);

	VkFramebufferCreateInfo framebufferCreateInfo;
	framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferCreateInfo.pNext = nullptr;
	framebufferCreateInfo.flags = 0;
	framebufferCreateInfo.renderPass = renderPass;
	framebufferCreateInfo.attachmentCount = 1;
	framebufferCreateInfo.pAttachments = &imageView;
	framebufferCreateInfo.width = surfaceCapabilities.currentExtent.width;
	framebufferCreateInfo.height = surfaceCapabilities.currentExtent.height;
	framebufferCreateInfo.layers = 1;

	VkFramebuffer framebuffer;
	result = vkCreateFramebuffer(pDevices[commandPoolDeviceIndex], &framebufferCreateInfo, nullptr, &framebuffer);

	if (result < 0)
		exit(1);

	VkClearValue clearValue;
	clearValue.color.uint32[0] = 255;
	clearValue.color.uint32[1] = 0;
	clearValue.color.uint32[2] = 0;
	clearValue.color.uint32[3] = 255;

	VkRenderPassBeginInfo renderPassBeginInfo;
	renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassBeginInfo.pNext = nullptr;
	renderPassBeginInfo.renderPass = renderPass;
	renderPassBeginInfo.framebuffer = framebuffer;
	renderPassBeginInfo.renderArea = { 0, 0, surfaceCapabilities.currentExtent.width, surfaceCapabilities.currentExtent.height };
	renderPassBeginInfo.clearValueCount = 1;
	renderPassBeginInfo.pClearValues = &clearValue;

	char *pVertexShaderCode;
	uint64_t vertexShaderCodeLength64;
	
	if (!_ReadFile("../../../shaders/test.vert.spv", &pVertexShaderCode, &vertexShaderCodeLength64))
		exit(1);

	uint32_t vertexShaderCodeLength32 = (uint32_t)vertexShaderCodeLength64;

	VkShaderModuleCreateInfo vertexShaderModuleCreateInfo;
	vertexShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	vertexShaderModuleCreateInfo.pNext = nullptr;
	vertexShaderModuleCreateInfo.flags = 0;
	vertexShaderModuleCreateInfo.codeSize = vertexShaderCodeLength32;
	vertexShaderModuleCreateInfo.pCode = (uint32_t*)pVertexShaderCode;

	VkShaderModule vertexShaderModule;
	result = vkCreateShaderModule(pDevices[commandPoolDeviceIndex], &vertexShaderModuleCreateInfo, nullptr, &vertexShaderModule);

	if (result < 0)
		exit(1);

	VkPipelineShaderStageCreateInfo vertexPipelinShaderStageCreateInfo;
	vertexPipelinShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertexPipelinShaderStageCreateInfo.pNext = nullptr;
	vertexPipelinShaderStageCreateInfo.flags = 0;
	vertexPipelinShaderStageCreateInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertexPipelinShaderStageCreateInfo.module = vertexShaderModule;
	vertexPipelinShaderStageCreateInfo.pName = "main";
	vertexPipelinShaderStageCreateInfo.pSpecializationInfo = nullptr;

	char *pFragmentShaderCode;
	uint64_t fragmentShaderCodeLength64;

	if (!_ReadFile("../../../shaders/test.frag.spv", &pFragmentShaderCode, &fragmentShaderCodeLength64))
		exit(1);

	uint32_t fragmentShaderCodeLength32 = (uint32_t)fragmentShaderCodeLength64;

	VkShaderModuleCreateInfo fragmentShaderModuleCreateInfo;
	fragmentShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	fragmentShaderModuleCreateInfo.pNext = nullptr;
	fragmentShaderModuleCreateInfo.flags = 0;
	fragmentShaderModuleCreateInfo.codeSize = fragmentShaderCodeLength32;
	fragmentShaderModuleCreateInfo.pCode = (uint32_t*)pFragmentShaderCode;

	VkShaderModule fragmentShaderModule;
	result = vkCreateShaderModule(pDevices[commandPoolDeviceIndex], &fragmentShaderModuleCreateInfo, nullptr, &fragmentShaderModule);

	if (result < 0)
		exit(1);

	VkPipelineShaderStageCreateInfo fragmentPipelinShaderStageCreateInfo;
	fragmentPipelinShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragmentPipelinShaderStageCreateInfo.pNext = nullptr;
	fragmentPipelinShaderStageCreateInfo.flags = 0;
	fragmentPipelinShaderStageCreateInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragmentPipelinShaderStageCreateInfo.module = fragmentShaderModule;
	fragmentPipelinShaderStageCreateInfo.pName = "main";
	fragmentPipelinShaderStageCreateInfo.pSpecializationInfo = nullptr;

	VkPipelineShaderStageCreateInfo pipelineShaderStageCreateInfos[] = { vertexPipelinShaderStageCreateInfo, fragmentPipelinShaderStageCreateInfo };

	VkVertexInputBindingDescription vertexInputBindingDescription;
	vertexInputBindingDescription.binding = 0;
	vertexInputBindingDescription.stride = sizeof(float) * 2;
	vertexInputBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	VkVertexInputAttributeDescription vertexInputAttributeDescription;
	vertexInputAttributeDescription.location = 0;
	vertexInputAttributeDescription.binding = 0;
	vertexInputAttributeDescription.format = VK_FORMAT_R32G32_SFLOAT;
	vertexInputAttributeDescription.offset = 0;

	VkPipelineVertexInputStateCreateInfo pipelineVertexInputStateCreateInfo;
	pipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	pipelineVertexInputStateCreateInfo.pNext = nullptr;
	pipelineVertexInputStateCreateInfo.flags = 0;
	pipelineVertexInputStateCreateInfo.vertexBindingDescriptionCount = 1;
	pipelineVertexInputStateCreateInfo.pVertexBindingDescriptions = &vertexInputBindingDescription;
	pipelineVertexInputStateCreateInfo.vertexAttributeDescriptionCount = 1;
	pipelineVertexInputStateCreateInfo.pVertexAttributeDescriptions = &vertexInputAttributeDescription;

	VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyStateCreateInfo;
	pipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	pipelineInputAssemblyStateCreateInfo.pNext = nullptr;
	pipelineInputAssemblyStateCreateInfo.flags = 0;
	pipelineInputAssemblyStateCreateInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	pipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = false;

	printf("Viewport bounds range: %.3f, %.3f\n", physicalDeviceProperties.limits.viewportBoundsRange[0], physicalDeviceProperties.limits.viewportBoundsRange[1]);
	printf("Max viewport dimensions: %d, %d\n", physicalDeviceProperties.limits.maxViewportDimensions[0], physicalDeviceProperties.limits.maxViewportDimensions[1]);

	VkViewport viewport;
	viewport.x = 0.f;
	viewport.y = 0.f;
	viewport.width = (float)surfaceCapabilities.currentExtent.width;
	viewport.height = (float)surfaceCapabilities.currentExtent.height;
	viewport.minDepth = 0.f;
	viewport.maxDepth = 1.f;

	VkRect2D scissors;
	scissors.offset.x = 0;
	scissors.offset.y = 0;
	scissors.extent.width = surfaceCapabilities.currentExtent.width;
	scissors.extent.height = surfaceCapabilities.currentExtent.height;

	VkPipelineViewportStateCreateInfo pipelineViewportStateCreateInfo;
	pipelineViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	pipelineViewportStateCreateInfo.pNext = nullptr;
	pipelineViewportStateCreateInfo.flags = 0;
	pipelineViewportStateCreateInfo.viewportCount = 1;
	pipelineViewportStateCreateInfo.pViewports = &viewport;
	pipelineViewportStateCreateInfo.scissorCount = 1;
	pipelineViewportStateCreateInfo.pScissors = &scissors;

	VkPipelineRasterizationStateCreateInfo pipelineRasterizationStateCreateInfo;
	pipelineRasterizationStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	pipelineRasterizationStateCreateInfo.pNext = nullptr;
	pipelineRasterizationStateCreateInfo.flags = 0;
	pipelineRasterizationStateCreateInfo.depthClampEnable = false;
	pipelineRasterizationStateCreateInfo.rasterizerDiscardEnable = false;
	pipelineRasterizationStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
	pipelineRasterizationStateCreateInfo.cullMode = VK_CULL_MODE_NONE;
	pipelineRasterizationStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	pipelineRasterizationStateCreateInfo.depthBiasEnable = false;
	pipelineRasterizationStateCreateInfo.depthBiasConstantFactor = 0.f;
	pipelineRasterizationStateCreateInfo.depthBiasClamp = 0.f;
	pipelineRasterizationStateCreateInfo.depthBiasSlopeFactor = 0.f;
	pipelineRasterizationStateCreateInfo.lineWidth = 1.f;

	VkPipelineMultisampleStateCreateInfo pipelineMultisampleStateCreateInfo;
	pipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	pipelineMultisampleStateCreateInfo.pNext = nullptr;
	pipelineMultisampleStateCreateInfo.flags = 0;
	pipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	pipelineMultisampleStateCreateInfo.sampleShadingEnable = false;
	pipelineMultisampleStateCreateInfo.minSampleShading = 0.f;
	pipelineMultisampleStateCreateInfo.pSampleMask = nullptr;
	pipelineMultisampleStateCreateInfo.alphaToCoverageEnable = false;
	pipelineMultisampleStateCreateInfo.alphaToOneEnable = false;

	VkPipelineDepthStencilStateCreateInfo pipelineDepthStencilStateCreateInfo;
	pipelineDepthStencilStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	pipelineDepthStencilStateCreateInfo.pNext = nullptr;
	pipelineDepthStencilStateCreateInfo.flags = 0;
	pipelineDepthStencilStateCreateInfo.depthTestEnable = false;
	pipelineDepthStencilStateCreateInfo.depthWriteEnable = false;
	pipelineDepthStencilStateCreateInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	pipelineDepthStencilStateCreateInfo.depthBoundsTestEnable = false;
	pipelineDepthStencilStateCreateInfo.stencilTestEnable = false;
	pipelineDepthStencilStateCreateInfo.front.failOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.front.passOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.front.depthFailOp = VK_STENCIL_OP_KEEP;
	pipelineDepthStencilStateCreateInfo.front.compareOp = VK_COMPARE_OP_ALWAYS;
	pipelineDepthStencilStateCreateInfo.front.compareMask = 0xFFFFFFFF;
	pipelineDepthStencilStateCreateInfo.front.writeMask = 0xFFFFFFFF;
	pipelineDepthStencilStateCreateInfo.front.reference = 0xFFFFFFFF;
	pipelineDepthStencilStateCreateInfo.back = pipelineDepthStencilStateCreateInfo.front;
	pipelineDepthStencilStateCreateInfo.minDepthBounds = 0.f;
	pipelineDepthStencilStateCreateInfo.maxDepthBounds = 1.f;


	VkPipelineColorBlendAttachmentState pipelineColorBlendAttachmentState;
	pipelineColorBlendAttachmentState.blendEnable = false;
	pipelineColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	pipelineColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	pipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
	pipelineColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	pipelineColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	pipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
	pipelineColorBlendAttachmentState.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	VkPipelineColorBlendStateCreateInfo pipelineColorBlendStateCreateInfo;
	pipelineColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	pipelineColorBlendStateCreateInfo.pNext = nullptr;
	pipelineColorBlendStateCreateInfo.flags = 0;
	pipelineColorBlendStateCreateInfo.logicOpEnable = false;
	pipelineColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_NO_OP;
	pipelineColorBlendStateCreateInfo.attachmentCount = 1;
	pipelineColorBlendStateCreateInfo.pAttachments = &pipelineColorBlendAttachmentState;
	pipelineColorBlendStateCreateInfo.blendConstants[0] = 0.f;
	pipelineColorBlendStateCreateInfo.blendConstants[1] = 0.f;
	pipelineColorBlendStateCreateInfo.blendConstants[2] = 0.f;
	pipelineColorBlendStateCreateInfo.blendConstants[3] = 0.f;

	//VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo;
	//descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	//descriptorSetLayoutCreateInfo.pNext = nullptr;
	//descriptorSetLayoutCreateInfo.flags = 0;
	//descriptorSetLayoutCreateInfo.bindingCount = 0;
	//descriptorSetLayoutCreateInfo.pBindings = nullptr;
	//
	//VkDescriptorSetLayout descriptorSetLayout;
	//vkCreateDescriptorSetLayout(pDevices[commandPoolDeviceIndex], &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout);

	VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo;
	pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutCreateInfo.pNext = nullptr;
	pipelineLayoutCreateInfo.flags = 0;
	pipelineLayoutCreateInfo.setLayoutCount = 0;
	pipelineLayoutCreateInfo.pSetLayouts = nullptr;
	pipelineLayoutCreateInfo.pushConstantRangeCount = 0;
	pipelineLayoutCreateInfo.pPushConstantRanges = nullptr;

	VkPipelineLayout pipelineLayout;
	result = vkCreatePipelineLayout(pDevices[commandPoolDeviceIndex], &pipelineLayoutCreateInfo, nullptr, &pipelineLayout);

	if (result < 0)
		exit(1);

	VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo;
	graphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	graphicsPipelineCreateInfo.pNext = nullptr;
	graphicsPipelineCreateInfo.flags = 0;
	graphicsPipelineCreateInfo.stageCount = 2;
	graphicsPipelineCreateInfo.pStages = pipelineShaderStageCreateInfos;
	graphicsPipelineCreateInfo.pVertexInputState = &pipelineVertexInputStateCreateInfo;
	graphicsPipelineCreateInfo.pInputAssemblyState = &pipelineInputAssemblyStateCreateInfo;
	graphicsPipelineCreateInfo.pTessellationState = nullptr;
	graphicsPipelineCreateInfo.pViewportState = &pipelineViewportStateCreateInfo;
	graphicsPipelineCreateInfo.pRasterizationState = &pipelineRasterizationStateCreateInfo;
	graphicsPipelineCreateInfo.pMultisampleState = &pipelineMultisampleStateCreateInfo;
	graphicsPipelineCreateInfo.pDepthStencilState = &pipelineDepthStencilStateCreateInfo;
	graphicsPipelineCreateInfo.pColorBlendState = &pipelineColorBlendStateCreateInfo;
	graphicsPipelineCreateInfo.pDynamicState = nullptr;
	graphicsPipelineCreateInfo.layout = pipelineLayout;
	graphicsPipelineCreateInfo.renderPass = renderPass;
	graphicsPipelineCreateInfo.subpass = 0;
	graphicsPipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	graphicsPipelineCreateInfo.basePipelineIndex = 0;

	VkPipeline graphicsPipeline;
	result = vkCreateGraphicsPipelines(pDevices[commandPoolDeviceIndex], VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, nullptr, &graphicsPipeline);

	if (result < 0)
		exit(1);

	VkQueue submitQueue;
	bool foundSubmitQueue;

	for (uint32_t i = 0, queueIndex = 0; i < pQueueFamilyCounts[commandPoolDeviceIndex]; queueIndex += pQueueFamilyCounts[i], ++i)
	{
		if ((pQueueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0)
		{
			submitQueue = pQueues[queueIndex];
			foundSubmitQueue = true;
		}
	}

	if (!foundSubmitQueue)
		exit(1);

	VkBuffer vertexBuffer;
	_CreateDeviceVertexBuffer(pDevices[commandPoolDeviceIndex], submitQueue, vertices, 3, nullptr, pPhysicalDeviceMemoryProperties + commandPoolDeviceIndex, commandPool, &vertexBuffer);

	/*VkBufferCreateInfo vertexBufferCreateInfo;
	vertexBufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	vertexBufferCreateInfo.pNext = nullptr;
	vertexBufferCreateInfo.flags = 0;
	vertexBufferCreateInfo.size = sizeof(float) * 2 * 3;
	vertexBufferCreateInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	vertexBufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	vertexBufferCreateInfo.queueFamilyIndexCount = 0;
	vertexBufferCreateInfo.pQueueFamilyIndices = nullptr;

	VkBuffer vertexBuffer;
	result = vkCreateBuffer(pDevices[commandPoolDeviceIndex], &vertexBufferCreateInfo, nullptr, &vertexBuffer);

	if (result < 0)
		exit(1);

	VkMemoryRequirements vertexBufferMemoryRequirements;
	vkGetBufferMemoryRequirements(pDevices[commandPoolDeviceIndex], vertexBuffer, &vertexBufferMemoryRequirements);
	
	if (!_GetMemoryAllocateInfoForMemoryRequirements(pPhysicalDeviceMemoryProperties + commandPoolDeviceIndex, &vertexBufferMemoryRequirements, &memoryAllocateInfo, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT))
		exit(1);

	VkDeviceMemory vertexBufferDeviceMemory;
	result = vkAllocateMemory(pDevices[commandPoolDeviceIndex], &memoryAllocateInfo, nullptr, &vertexBufferDeviceMemory);

	if (result < 0)
		exit(1);

	float *pMappedVertexBuffer;
	result = vkMapMemory(pDevices[commandPoolDeviceIndex], vertexBufferDeviceMemory, 0, vertexBufferMemoryRequirements.size, 0, (void**)&pMappedVertexBuffer);

	if (result < 0)
		exit(1);

	memcpy(pMappedVertexBuffer, vertices, sizeof(float) * 2 * 3);

	vkUnmapMemory(pDevices[commandPoolDeviceIndex], vertexBufferDeviceMemory);

	VkMappedMemoryRange vertexBufferMappedMemoryRange;
	vertexBufferMappedMemoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	vertexBufferMappedMemoryRange.pNext = nullptr;
	vertexBufferMappedMemoryRange.memory = vertexBufferDeviceMemory;
	vertexBufferMappedMemoryRange.offset = 0;
	vertexBufferMappedMemoryRange.size = VK_WHOLE_SIZE;

	vkInvalidateMappedMemoryRanges(pDevices[commandPoolDeviceIndex], 1, &vertexBufferMappedMemoryRange);

	result = vkBindBufferMemory(pDevices[commandPoolDeviceIndex], vertexBuffer, vertexBufferDeviceMemory, 0);

	if (result < 0)
		exit(1);*/

	//VkImageMemoryBarrier preImageMemoryBarrier;
	//preImageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	//preImageMemoryBarrier.pNext = nullptr;
	//preImageMemoryBarrier.srcAccessMask = 0;
	//preImageMemoryBarrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	//preImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	//preImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	//preImageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	//preImageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	//preImageMemoryBarrier.image = pSwapchainImages[swapchainNextImageIndex];
	//preImageMemoryBarrier.subresourceRange = imageSubresourceRange;

	VkImageMemoryBarrier imageMemoryBarrier;
	imageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	imageMemoryBarrier.pNext = nullptr;
	imageMemoryBarrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	imageMemoryBarrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
	imageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	imageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	imageMemoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	imageMemoryBarrier.image = pSwapchainImages[swapchainNextImageIndex];
	imageMemoryBarrier.subresourceRange = imageSubresourceRange;

	VkDeviceSize vertexBufferOffset = 0;

	VkCommandBufferBeginInfo commandBufferBeginInfo;
	commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	commandBufferBeginInfo.pNext = nullptr;
	commandBufferBeginInfo.flags = 0;
	commandBufferBeginInfo.pInheritanceInfo = nullptr;

	result = vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo);

	if (result < 0)
		exit(1);

	//vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &preImageMemoryBarrier);
	vkCmdBeginRenderPass(commandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
	vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
	vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertexBuffer, &vertexBufferOffset);
	vkCmdDraw(commandBuffer, 3, 1, 0, 0);
	vkCmdEndRenderPass(commandBuffer);
	vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &imageMemoryBarrier);
	result = vkEndCommandBuffer(commandBuffer);

	if (result < 0)
		exit(1);

	VkSemaphoreCreateInfo queueSignalSemaphoreCreateInfo;
	queueSignalSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
	queueSignalSemaphoreCreateInfo.pNext = nullptr;
	queueSignalSemaphoreCreateInfo.flags = 0;

	VkSemaphore queueSignalSemaphore;
	vkCreateSemaphore(pDevices[commandPoolDeviceIndex], &queueSignalSemaphoreCreateInfo, nullptr, &queueSignalSemaphore);

	if (result < 0)
		exit(1);

	VkSubmitInfo queueSubmitInfo;
	queueSubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	queueSubmitInfo.pNext = nullptr;
	queueSubmitInfo.waitSemaphoreCount = 0;
	queueSubmitInfo.pWaitSemaphores = nullptr;
	queueSubmitInfo.pWaitDstStageMask = nullptr;
	queueSubmitInfo.commandBufferCount = 1;
	queueSubmitInfo.pCommandBuffers = &commandBuffer;
	queueSubmitInfo.signalSemaphoreCount = 1;
	queueSubmitInfo.pSignalSemaphores = &queueSignalSemaphore;

	VkFenceCreateInfo submitFenceCreateInfo;
	submitFenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	submitFenceCreateInfo.pNext = nullptr;
	submitFenceCreateInfo.flags = 0;

	VkFence submitFence;
	result = vkCreateFence(pDevices[commandPoolDeviceIndex], &submitFenceCreateInfo, nullptr, &submitFence);

	if (result < 0)
		exit(1);

	result = vkQueueSubmit(submitQueue, 1, &queueSubmitInfo, submitFence);

	if (result < 0)
		exit(1);

	result = vkWaitForFences(pDevices[commandPoolDeviceIndex], 1, &submitFence, true, 1000000);

	if (result < 0)
		exit(1);

	VkPresentInfoKHR presentInfo;
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.pNext = nullptr;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &queueSignalSemaphore;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &swapchain;
	presentInfo.pImageIndices = &swapchainNextImageIndex;
	presentInfo.pResults = &result;

	
	result = vkQueuePresentKHR(submitQueue, &presentInfo);

	if (result < 0)
		exit(1);

	MSG message;
	
	while (GetMessage(&message, nullptr, 0, 0) && message.message != WM_QUIT)
		DispatchMessage(&message);
}