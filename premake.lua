solution "HelloVulkan"
	
	configurations { "Debug", "Release" }
	platforms { "x32", "x64" }
	
	flags { "Symbols", "ExtraWarnings", "FatalWarnings" }
	
	project "HelloVulkan"
		kind "ConsoleApp"
		language "C++"
		includedirs { "./3rdparty/vulkan/include" }
		files { "**.h", "**.cpp", "premake.lua" }
		links { "vulkan-1.lib" }
		
		libdirs { "./3rdparty/vulkan/lib/%{cfg.platform}" }
		targetdir "build/%{cfg.buildcfg}/%{cfg.platform}"
		debugdir "build/%{cfg.buildcfg}/%{cfg.platform}"